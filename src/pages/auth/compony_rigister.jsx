import {
    Apartment,
    ArrowBackIosNew,
    BusinessCenter,
    ContentPasteSearch,
    LoginOutlined,
  } from "@mui/icons-material";
  import { Button, Stack, Typography } from "@mui/material";
  import React from "react";
  import { useNavigate } from "react-router-dom";
  const Rigister = () => {
      const navigate = useNavigate()
      const handlechange= ()=>{
          navigate('/worker/login')
  
      }
    return (
      <Stack
        direction={"column"}
        alignItems={"center"}
        sx={{ height: "100vh", background: "#f5f7f9" }}
      >
        <Stack
          width={"600px"}
          direction={"row"}
          sx={{ justifyContent: "space-between", padding: " 50px" }}
        >
          <Button
            sx={{
              color: "#001444",
              textTransform: "capitalize",
              fontWeight: "bold",
            }}
            variant="text"
          >
            <ArrowBackIosNew fontSize="12px" /> Home
          </Button>
          <img src="https://jobhunt.uz/img/logo/register_logo_blue.svg" alt="" />
          <select
            style={{
              padding: "12px",
              borderRight: "15px solid #e2e5ea",
              background: "#e2e5ea",
              borderColor: "#e2e5ea",
              borderRadius: "10px",
            }}
          >
            <option value="Uzb">O'zbekcha</option>
            <option value="rus">Russian</option>
          </select>
        </Stack>

        <Stack width={"600px"} direction={"column"} sx={{ background: "#fff" }}>
          <Stack
            direction={"row"}
            justifyContent={"space-between"}
            sx={{
              margin: "30px 25px",
              paddingBottom: "25px",
              borderBottom: "2px solid #e5e5e5",
            }}
          >
            <Typography
              variant="h4"
              fontWeight={"bold"}
              sx={{ color: "#001444" }}
            >Rigister
              
            </Typography>
            <button
            onClick={handlechange}
              style={{
                display: "flex",
                alignItems: "center",
                padding: "10px",
                border: "1px solid #e2e5ea",
                background: "#fff",
                borderRadius: "8px",
              }}
            >
              <LoginOutlined sx={{ marginRight: "10px" }} />
              <Typography>To come in</Typography>
            </button>
          </Stack>
          <Stack>
           
            <Stack direction={"row"} sx={{padding: "25px"}} justifyContent={'space-between'}>
                <button
                  style={{
                    padding: "70px 85px",
                    background: "#fff",
                    border: "1px solid #e0eafa",
                    borderTopLeftRadius: "6px",
                    borderRadius: "6px",
                    cursor: 'pointer'
                  }}
                >
                  <BusinessCenter sx={{ color: "#0062ff", fontSize: '60px' }} />
                </button>
                <button
                  style={{
                    padding: "70px 85px",
                    border: "1px solid #e0eafa",
                    borderTopRightRadius: "6px",
                    borderRadius: "6px",
                    background: "#fff",
                    cursor: 'pointer'
  
                  }}
                >
                  <Apartment sx={{ color: "#00a795", fontSize: "60px" }} />
                </button>
             
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    );
  };
  
  export default Rigister;
  