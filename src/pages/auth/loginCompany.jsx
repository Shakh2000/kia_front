import {
  Apartment,
  ArrowBackIosNew,
  BusinessCenter,
  ContentPasteSearch,
  LoginOutlined,
} from "@mui/icons-material";
import { Button, Stack, Typography } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
const LoginCompany = () => {
    const navigate = useNavigate()
    const handlechange= ()=>{
        navigate('/worker/login')

    }
    const handleNavigate = () =>{
        navigate('/register')
    }
  return (
    <Stack
      direction={"column"}
      alignItems={"center"}
      sx={{ height: "100vh", background: "#f5f7f9" }}
    >
      <Stack
        width={"600px"}
        direction={"row"}
        sx={{ justifyContent: "space-between", padding: " 50px" }}
      >
        <Button
          sx={{
            color: "#001444",
            textTransform: "capitalize",
            fontWeight: "bold",
          }}
          variant="text"
        >
          <ArrowBackIosNew fontSize="12px" /> Home
        </Button>
        <img src="https://jobhunt.uz/img/logo/register_logo_green.svg" alt="" />
        <select
          style={{
            padding: "12px",
            borderRight: "15px solid #e2e5ea",
            background: "#e2e5ea",
            borderColor: "#e2e5ea",
            borderRadius: "10px",
          }}
        >
          <option value="Uzb">O'zbekcha</option>
          <option value="rus">Russian</option>
        </select>
      </Stack>
      <Stack
        width={"600px"}
        sx={{ background: "#00a795", padding: "5px 0", borderRadius: "10px" }}
      ></Stack>
      <Stack width={"600px"} direction={"column"} sx={{ background: "#fff" }}>
        <Stack
          direction={"row"}
          justifyContent={"space-between"}
          sx={{
            margin: "30px 25px",
            paddingBottom: "25px",
            borderBottom: "2px solid #e5e5e5",
          }}
        >
          <Typography
            variant="h4"
            fontWeight={"bold"}
            sx={{ color: "#001444" }}
          >
            To come in
          </Typography>
          <button
          onClick={handleNavigate}
            style={{
              display: "flex",
              alignItems: "center",
              padding: "10px",
              border: "1px solid #e2e5ea",
              background: "#fff",
              borderRadius: "8px",
            }}
          >
            <LoginOutlined sx={{ marginRight: "10px" }} />
            <Typography>Rigister</Typography>
          </button>
        </Stack>
        <Stack>
          <label
            style={{
              color: "#8c97ab",
              fontWeight: "bold",
              padding: "10px 25px",
              marginTop: "-10px",
            }}
          >
            Username
          </label>
          <input
            className="inputs"
            style={{
              width: "92%",
              margin: "auto",
              fontSize: "18px",
              padding: "14px 20px",
              border: "2px solid #eaebef",
              borderRadius: "8px",
            }}
            type="tel"
            placeholder="+998 91 599 99 99"
          />
          <label
            style={{
              color: "#8c97ab",
              fontWeight: "bold",
              padding: "10px 25px",
              marginTop: "10px",
            }}
          >
            Password
          </label>
          <input
            className="inputs"
            style={{
              width: "92%",
              margin: "auto",
              fontSize: "18px",
              padding: "14px 20px",
              border: "2px solid #eaebef",
              borderRadius: "8px",
              color: "#8c97ab",
            }}
            type="password"
            placeholder="Enter your password"
          />
          <Stack
            direction={"row"}
            sx={{ padding: "25px" }}
            justifyContent={"space-between"}
          >
            <div>
              <button
                onClick={handlechange}
                style={{
                  padding: "5px 8px",
                  background: "#fff",
                  border: "1px solid #d3f3f0",
                  borderTopLeftRadius: "6px",
                  borderBottomLeftRadius: "6px",
                  cursor: "pointer",
                }}
              >
                <BusinessCenter sx={{ color: "#a9b1c0" }} />
              </button>
              <button
                style={{
                  padding: "5px 8px",
                  border: "1px solid #e0eafa",
                  borderTopRightRadius: "6px",
                  borderBottomRightRadius: "6px",
                  background: "#d3f3f0",
                  cursor: "pointer",
                }}
              >
                <Apartment sx={{ color: "#00a795" }} />
              </button>
            </div>
            <Button
              variant="contained"
              sx={{
                textTransform: "capitalize",
                background: "#00a795",
                padding: "8px 35px",
              }}
            >
              To come in
            </Button>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default LoginCompany;
