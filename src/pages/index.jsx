import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

const Auth = () => {
    const navigate = useNavigate()
    useEffect(()=>{
        navigate("/worker/login")
    }, [])
  return (
    <div>Auth</div>
  )
}

export default Auth