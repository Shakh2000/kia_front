import "./App.css";
import { Route, Routes } from "react-router-dom";
import Login from "./pages/auth/login";
import { useState } from "react";
import Auth from "./pages";
import LoginCompany from "./pages/auth/loginCompany";
import Rigister from "./pages/auth/compony_rigister";

function App() {
  return (
    <div className="app">
      <main className="content">
        <Routes>
          <Route path="/" element={<Auth />} />
          <Route path="/worker/login" element={<Login />} />
          <Route path="/company/login" element={<LoginCompany />} />
          <Route path="/register" element={<Rigister />} />
          
        </Routes>
      </main>
    </div>
  );
}

export default App;
